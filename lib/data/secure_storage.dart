import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:happy_chat/constants/constants.dart';

class SecureStorage {
  static FlutterSecureStorage _storage = FlutterSecureStorage();

  static Future<void> setId(String id) async {
    await _storage.write(key: userIdKey, value: id);
  }

  static Future<void> setToken(String accessToken) async {
    await _storage.write(key: accessTokenKey, value: accessToken);
  }

  // static Future<void> getToken(String key) async {
  //   await _storage.read(key: key);
  // }

  static Future<String?> getToken(String key) async {
    return await _storage.read(key: key);
  }

  static Future<void> deleteToken(String key) async {
    await _storage.delete(key: key);
  }
}
