import 'package:flutter/material.dart';

class ButtonDefault extends StatelessWidget {
  final String buttonText;
  final void Function() onTap;
  final bool isLoading;
  final bool disabled;
  const ButtonDefault({
    Key? key,
    required this.buttonText,
    required this.onTap,
    required this.disabled,
    this.isLoading = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width - 20,
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: disabled ? Color.fromRGBO(0, 0, 0, 0.2) : Color(0xFF161616),
        ),
        child: isLoading
            ? CircularProgressIndicator(
                color: Colors.white,
                strokeWidth: 2,
              )
            : Text(
                buttonText,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Color(0xFFF1F1F1),
                ),
              ),
      ),
    );
  }
}
