import 'package:flutter/material.dart';

class ChatBoxInput extends StatelessWidget {
  final TextEditingController messageTypeController;
  final void Function() onSendButton;
  final String textInputHint;
  const ChatBoxInput({
    Key? key,
    required this.messageTypeController,
    required this.onSendButton,
    required this.textInputHint,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      color: Color(0xFFFCE8E6),
      width: MediaQuery.of(context).size.width,
      height: 60,
      child: Row(
        children: [
          GestureDetector(
            onTap: onSendButton,
            child: Icon(
              Icons.send_rounded,
              color: Color(0xFF696969),
            ),
          ),
          Expanded(
            child: TextField(
              controller: messageTypeController,
              textAlign: TextAlign.right,
              style: TextStyle(
                fontWeight: FontWeight.w700,
              ),
              decoration: InputDecoration(
                hintText: textInputHint,
                border: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
