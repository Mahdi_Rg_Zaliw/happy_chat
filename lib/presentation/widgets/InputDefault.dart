import 'package:flutter/material.dart';

class InputDefault extends StatelessWidget {
  final String hintText;
  final void Function(String) onChangeInput;
  final TextEditingController textInputController;
  final Color borderColor;
  const InputDefault({
    Key? key,
    required this.hintText,
    required this.textInputController,
    required this.onChangeInput,
    required this.borderColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 30,
      height: 60,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          width: 1,
          color: borderColor,
        ),
      ),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 15),
            width: 35,
            height: 25,
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/Flag_of_Iran.svg/2560px-Flag_of_Iran.svg.png'))),
          ),
          Container(
            margin: EdgeInsets.only(right: 10),
            width: 1,
            height: 45,
            color: Color(0xFFD1D1D1),
          ),
          Expanded(
            child: TextField(
              maxLength: 14,
              onChanged: onChangeInput,
              controller: textInputController,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                counterText: '',
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: TextStyle(
                  color: Color(0xFFD1D1D1),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
