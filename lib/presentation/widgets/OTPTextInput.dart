import 'package:flutter/material.dart';

class OTPTextInput extends StatefulWidget {
  final bool error;
  final FocusNode focusNode;
  final TextEditingController otpController;
  final void Function(String) onChangeText;
  const OTPTextInput({
    Key? key,
    required this.onChangeText,
    required this.focusNode,
    required this.otpController,
    required this.error,
  }) : super(key: key);

  @override
  _OTPTextInputState createState() => _OTPTextInputState();
}

class _OTPTextInputState extends State<OTPTextInput> {
  Color borderColor = Color(0xFFD1D1D1);
  @override
  void initState() {
    super.initState();
    widget.focusNode.addListener(() {
      if (widget.focusNode.hasFocus) {
        setState(() {
          borderColor = Color(0xFF161616);
        });
      } else {
        setState(() {
          borderColor = Color(0xFFD1D1D1);
        });
      }
    });

    if (widget.error) {
      setState(() {
        borderColor = Color(0xFFEA2E38);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width / 5,
      height: MediaQuery.of(context).size.width / 5,
      margin: EdgeInsets.symmetric(horizontal: 6),
      padding: EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
          color: widget.error ? Color(0xFFEA2E38) : borderColor,
          width: 1,
        ),
      ),
      child: TextField(
        keyboardType: TextInputType.number,
        controller: widget.otpController,
        focusNode: widget.focusNode,
        onChanged: widget.onChangeText,
        textAlign: TextAlign.center,
        maxLength: 1,
        style: TextStyle(
          fontSize: 34,
        ),
        decoration: InputDecoration(
          counterText: '',
          border: InputBorder.none,
        ),
      ),
    );
  }
}
