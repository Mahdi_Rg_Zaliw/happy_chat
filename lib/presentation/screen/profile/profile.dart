import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:happy_chat/constants/constants.dart';
import 'package:happy_chat/data/secure_storage.dart';
import 'package:happy_chat/presentation/screen/chat/chat.dart';
import 'package:happy_chat/presentation/widgets/profile_item.dart';
import 'package:http/http.dart' as http;

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  List contactList = [];

  @override
  void initState() {
    super.initState();
    getContactListRequest();
  }

  Future getContactListRequest() async {
    var token = await SecureStorage.getToken(accessTokenKey);
    print('Token: ' + '$token');
    return http.get(
        Uri.parse('https://factor.behtarino.com/utils/challenge/contact_list/'),
        headers: {
          HttpHeaders.authorizationHeader: 'Token $token'
        }).then((value) => {
          print(jsonDecode(value.body)['data']),
          setState(() {
            contactList.addAll(jsonDecode(value.body)['data']);
          }),
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: profileHeader(context),
      body: contactList == []
          ? Container()
          : ListView(
              children: contactList
                  .map(
                    (e) => ProfileItem(
                      onTapItem: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => ChatScreen(
                            userName: e['name'],
                            userToken: e['token'],
                          ),
                        ),
                      ),
                      contactName: e['name'],
                    ),
                  )
                  .toList(),
            ),
    );
  }
}

PreferredSize profileHeader(BuildContext context) {
  return PreferredSize(
    preferredSize: Size(double.infinity, kToolbarHeight + 40),
    child: Column(
      children: [
        SizedBox(
          height: 40,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            children: [
              GestureDetector(
                onTap: () async {
                  await SecureStorage.deleteToken(accessTokenKey);
                  var token = await SecureStorage.getToken(accessTokenKey);
                  print(token);
                  Navigator.pushNamed(context, 'splash');
                },
                child: Container(
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    image: DecorationImage(
                      image: NetworkImage(
                          'https://static.vecteezy.com/system/resources/previews/002/275/847/original/male-avatar-profile-icon-of-smiling-caucasian-man-vector.jpg'),
                    ),
                  ),
                ),
              ),
              Expanded(child: Container()),
              IconButton(
                splashRadius: 28,
                onPressed: () => {},
                icon: Icon(Icons.search_outlined),
              ),
            ],
          ),
        ),
        SizedBox(height: 30),
      ],
    ),
  );
}
