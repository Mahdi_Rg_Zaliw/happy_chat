import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_chat/presentation/widgets/OTPTextInput.dart';
import 'package:http/http.dart' as http;
import 'package:happy_chat/data/secure_storage.dart';

class OTP extends StatefulWidget {
  final String phoneNumber;
  OTP({Key? key, required this.phoneNumber}) : super(key: key);

  @override
  _OTPState createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  bool otpComplated = false;
  late Timer timer;
  int _start = 10;
  bool errorCodeVerification = false;

  List focusNodeList = [
    FocusNode(),
    FocusNode(),
    FocusNode(),
    FocusNode(),
  ];

  List<TextEditingController> otpInputController = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
  ];

  String codeVerification = '';

  Future codeVerificationRequest(BuildContext context) {
    setState(() {
      otpComplated = true;
    });
    return http.post(
      Uri.parse('https://factor.behtarino.com/api/v1/token_sign/'),
      body: {'username': widget.phoneNumber, 'password': codeVerification},
    ).then(
      (value) async => {
        if (value.statusCode == 200)
          {
            setState(() {
              otpComplated = false;
            }),
            if (jsonDecode(value.body)['data'] != null)
              {
                await SecureStorage.setToken(
                    jsonDecode(value.body)['data']['token']),
                Navigator.pushReplacementNamed(context, 'profile'),
                setState(() {
                  errorCodeVerification = false;
                }),
              }
            else
              {
                setState(() {
                  errorCodeVerification = true;
                }),
              }
          }
        else
          {
            setState(() {
              otpComplated = false;
              errorCodeVerification = true;
            }),
          }
      },
    );
  }

  Future loginRequest() {
    setState(() {
      otpComplated = true;
    });
    return http.post(
      Uri.parse(
          'https://factor.behtarino.com/api/v1/users/phone_verification/'),
      body: {'phone': widget.phoneNumber},
    ).then(
      (value) => {
        if (value.statusCode == 200)
          {
            setState(() {
              otpComplated = false;
            }),
          }
        else
          {
            setState(() {
              otpComplated = false;
            }),
            print('Phone Number Wrong'),
          }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  void startTimer() {
    loginRequest();
    setState(() {
      codeVerification = '';
    });
    otpInputController[0].clear();
    otpInputController[1].clear();
    otpInputController[2].clear();
    otpInputController[3].clear();
    const oneSec = const Duration(seconds: 1);
    timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: [
          Positioned(
            right: 15,
            top: 45,
            child: GestureDetector(
              onTap: () => Navigator.pop(context),
              child: Row(
                children: [
                  Text(
                    'بازگشت',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
                  ),
                  SizedBox(width: 5),
                  Container(
                    alignment: Alignment.center,
                    width: 30,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Color(0xFFE1E1E1),
                      borderRadius: BorderRadius.circular(30),
                    ),
                    child: Icon(Icons.arrow_forward),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'هپی چت',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 44,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'برای ثبت نام کد 4 رقمی ارسال شده را وارد نمایید',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: EdgeInsets.only(
                    bottom: 10,
                    right: 8,
                    left: 8,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      OTPTextInput(
                        error: errorCodeVerification,
                        otpController: otpInputController[0],
                        onChangeText: (String value) {
                          codeVerification =
                              '${otpInputController[0].text}${otpInputController[1].text}${otpInputController[2].text}${otpInputController[3].text}';
                          if (value != '') {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[1]);
                          } else {
                            FocusScope.of(context).unfocus();
                          }

                          if (codeVerification.length == 4) {
                            codeVerificationRequest(context);
                          }
                        },
                        focusNode: focusNodeList[0],
                      ),
                      OTPTextInput(
                        error: errorCodeVerification,
                        otpController: otpInputController[1],
                        onChangeText: (String value) {
                          codeVerification =
                              '${otpInputController[0].text}${otpInputController[1].text}${otpInputController[2].text}${otpInputController[3].text}';
                          if (value != '') {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[2]);
                          } else {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[0]);
                          }

                          if (codeVerification.length == 4) {
                            codeVerificationRequest(context);
                          }
                        },
                        focusNode: focusNodeList[1],
                      ),
                      OTPTextInput(
                        error: errorCodeVerification,
                        otpController: otpInputController[2],
                        onChangeText: (String value) {
                          codeVerification =
                              '${otpInputController[0].text}${otpInputController[1].text}${otpInputController[2].text}${otpInputController[3].text}';
                          if (value != '') {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[3]);
                          } else {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[1]);
                          }

                          if (codeVerification.length == 4) {
                            codeVerificationRequest(context);
                          }
                        },
                        focusNode: focusNodeList[2],
                      ),
                      OTPTextInput(
                        error: errorCodeVerification,
                        otpController: otpInputController[3],
                        onChangeText: (String value) {
                          codeVerification =
                              '${otpInputController[0].text}${otpInputController[1].text}${otpInputController[2].text}${otpInputController[3].text}';
                          if (value != '') {
                            FocusScope.of(context).unfocus();
                          } else {
                            FocusScope.of(context)
                                .requestFocus(focusNodeList[2]);
                          }

                          if (codeVerification.length == 4) {
                            codeVerificationRequest(context);
                          }
                        },
                        focusNode: focusNodeList[3],
                      ),
                    ],
                  ),
                ),
                errorCodeVerification
                    ? Text(
                        'کد تاییدی که ارسال کرده اید نامعتبر است',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Color(0xFFEA2E38),
                        ),
                      )
                    : Container(),
                otpComplated == true
                    ? Container(
                        margin: EdgeInsets.symmetric(vertical: 10),
                        child: CircularProgressIndicator(
                          color: Color(0xFFF2B8BF),
                          strokeWidth: 2,
                        ),
                      )
                    : _start == 0
                        ? TextButton(
                            onPressed: () => {
                              print(codeVerification),
                              setState(() {
                                _start = 10;
                              }),
                              startTimer(),
                            },
                            child: Text(
                              'ارسال مجدد کد',
                              style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Color(0xFF161616),
                                decorationColor: Color(0xFF161616),
                                fontSize: 15,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          )
                        : Text(
                            'ارسال مجدد تا $_start ثانیه دیگر',
                            style: TextStyle(
                              color: Color(0xFF161616),
                              decorationColor: Color(0xFF161616),
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
              ],
            ),
          ),
          // Positioned(
          //   child: Container(
          //     padding: EdgeInsets.symmetric(horizontal: 10),
          //     width: MediaQuery.of(context).size.width,
          //     child: ButtonDefault(
          //       onTap: () => {},
          //       buttonText: 'ثبت نام',
          //     ),
          //   ),
          //   bottom: 20,
          // )
        ],
      ),
    );
  }
}
