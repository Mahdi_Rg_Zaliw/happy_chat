import 'package:flutter/material.dart';
import 'package:happy_chat/constants/constants.dart';
import 'package:happy_chat/data/secure_storage.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<String?> getToken() async {
    var accessToken = await SecureStorage.getToken(accessTokenKey);
    print(accessToken);
    if (accessToken != null) {
      Navigator.pushReplacementNamed(context, 'profile');
    } else {
      Navigator.pushReplacementNamed(context, 'login');
    }
  }

  @override
  void initState() {
    super.initState();
    getToken();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Happy Chat Splash',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: 15,
            ),
            CircularProgressIndicator(
              color: Color(0xFF161616),
            ),
          ],
        ),
      ),
    );
  }
}
