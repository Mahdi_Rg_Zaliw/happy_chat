import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:happy_chat/presentation/screen/auth/OTP.dart';
import 'package:happy_chat/presentation/widgets/ButtonDefault.dart';
import 'package:happy_chat/presentation/widgets/InputDefault.dart';
import 'package:http/http.dart' as http;

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController phoneNumberController = TextEditingController();
  bool textInputIsEmpty = true;
  bool vallidation = true;
  bool isLoading = false;
  Future loginRequest(BuildContext context) {
    setState(() {
      isLoading = true;
    });
    return http.post(
      Uri.parse(
          'https://factor.behtarino.com/api/v1/users/phone_verification/'),
      body: {'phone': phoneNumberController.text},
    ).then(
      (value) => {
        if (value.statusCode == 200)
          {
            setState(() {
              isLoading = false;
            }),
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => OTP(
                  phoneNumber: phoneNumberController.text,
                ),
              ),
            )
          }
        else
          {
            setState(() {
              isLoading = false;
            }),
            print('Phone Number Wrong'),
          }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'هپی چت',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 44,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                SizedBox(height: 30),
                Text(
                  'برای ثبت نام شماره تلفن خود را وارد کنید',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 13),
                Padding(
                  padding: EdgeInsets.only(bottom: 70),
                  child: Column(
                    children: [
                      InputDefault(
                        borderColor:
                            vallidation ? Color(0xFFD1D1D1) : Color(0xFFEA2E38),
                        onChangeInput: (String value) {
                          if (value != '') {
                            setState(() {
                              textInputIsEmpty = false;
                            });
                          } else {
                            setState(() {
                              textInputIsEmpty = true;
                            });
                          }
                        },
                        textInputController: phoneNumberController,
                        hintText: 'لطفا شماره تلفن خود را وارد کنید',
                      ),
                      vallidation
                          ? Container()
                          : Padding(
                              padding: EdgeInsets.only(top: 13),
                              child: Text(
                                'شماره تلفن معتبر نیست , لطفا مجددا تلاش کنید',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Color(0xFFEA2E38),
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: MediaQuery.of(context).size.width,
              child: ButtonDefault(
                isLoading: isLoading,
                disabled: textInputIsEmpty ? true : false,
                onTap: () => {
                  if (phoneNumberController.text.length <= 9)
                    {
                      setState(() {
                        vallidation = false;
                      }),
                    }
                  else
                    {
                      setState(() {
                        vallidation = true;
                      }),
                      loginRequest(context),
                    }
                },
                buttonText: 'ثبت نام',
              ),
            ),
            bottom: 20,
          )
        ],
      ),
    );
  }
}
