import 'package:flutter/material.dart';
import 'package:happy_chat/constants/constants.dart';
import 'package:happy_chat/data/secure_storage.dart';
import 'package:happy_chat/presentation/widgets/chat_box_input.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

class ChatScreen extends StatefulWidget {
  final String userName;
  final String userToken;
  ChatScreen({Key? key, required this.userName, required this.userToken})
      : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

Widget notExistMessage(BuildContext context) {
  return Container(
    alignment: Alignment.center,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Image(
          width: 150,
          height: 150,
          image: NetworkImage(
              'https://i.pinimg.com/originals/98/29/21/9829215db6f9210c0ae4e318e854cb1f.png'),
        ),
        Text('پیامی وجود ندارد'),
      ],
    ),
  );
}

// f2be8a
// 0xFFED9DA7

Widget messages(BuildContext context, String text, Color colorText) {
  return Container(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
    width: MediaQuery.of(context).size.width / 1.8,
    decoration: BoxDecoration(
      color: colorText,
      borderRadius: BorderRadius.circular(9),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          text,
          textAlign: TextAlign.right,
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w700,
            color: Color(0xFF161616),
          ),
        )
      ],
    ),
  );
}

class _ChatScreenState extends State<ChatScreen> {
  void mqttBrokerConnect() async {
    var otpToken = await SecureStorage.getToken(accessTokenKey);
    MqttServerClient mqttClient =
        MqttServerClient.withPort('185.86.181.206', 'happy_chat', 31789);
    mqttClient
        .connect('challenge', '8dAtPHvjPNC4erjFRfy')
        .then((value) => print('Connected: $value'));

    // mqttClient.publishMessage(
    //   'challenge/user/$otpToken/${widget.userToken}/',
    //   qualityOfService,
    //   data,
    // );
  }

  TextEditingController messageTypeController = TextEditingController();
  List messageList = [
    // {"user": 'A', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'B', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'A', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'B', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'A', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'B', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'A', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
    // {"user": 'B', "text": 'سلام من مهدی زالی قهی هستم', "date": "26 تیر"},
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: chatHeader(context, widget.userName),
      body: Column(
        children: [
          Expanded(
            child: messageList.length <= 0
                ? notExistMessage(context)
                : Container(
                    width: MediaQuery.of(context).size.width,
                    child: SingleChildScrollView(
                      reverse: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: messageList
                            .map(
                              (e) => Container(
                                width: MediaQuery.of(context).size.width,
                                alignment: e['user'] == 'B'
                                    ? Alignment.centerRight
                                    : Alignment.centerLeft,
                                child: messages(
                                  context,
                                  e['text'],
                                  e['user'] == 'B'
                                      ? Color(0xFFF2B8BF)
                                      : Color(0xFFF5C698),
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ),
                  ),
          ),
          ChatBoxInput(
            messageTypeController: messageTypeController,
            onSendButton: () => {
              mqttBrokerConnect(),
              if (messageTypeController.text.trim() != '')
                {
                  setState(() {
                    messageList.add({
                      'text': messageTypeController.text,
                      'user': 'A',
                      'date': '30 تیر'
                    });
                  }),
                  messageTypeController.clear(),
                }
            },
            textInputHint: 'نوشتن پیام',
          )
        ],
      ),
    );
  }
}

PreferredSize chatHeader(BuildContext context, String userName) {
  return PreferredSize(
    preferredSize: Size(double.infinity, kToolbarHeight + 40),
    child: Column(
      children: [
        SizedBox(
          height: 40,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 12),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  image: DecorationImage(
                    image: NetworkImage(
                        'https://static.vecteezy.com/system/resources/previews/002/275/847/original/male-avatar-profile-icon-of-smiling-caucasian-man-vector.jpg'),
                  ),
                ),
              ),
              Text(
                userName,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
              ),
              GestureDetector(
                onTap: () => Navigator.pop(context),
                child: Row(
                  children: [
                    Text(
                      'بازگشت',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w800),
                    ),
                    SizedBox(width: 5),
                    Container(
                      alignment: Alignment.center,
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Color(0xFFE1E1E1),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      child: Icon(Icons.arrow_forward),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 30),
      ],
    ),
  );
}
