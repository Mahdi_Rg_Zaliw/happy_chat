import 'package:flutter/material.dart';
import 'package:happy_chat/presentation/screen/auth/login.dart';
import 'package:happy_chat/presentation/screen/auth/splash.dart';
import 'package:happy_chat/presentation/screen/profile/profile.dart';

Map<String, Widget Function(BuildContext)> routes = {
  "login": (BuildContext context) => LoginScreen(),
  "splash": (BuildContext context) => SplashScreen(),
  // "otp_page": (BuildContext context) => OTP(),
  "profile": (BuildContext context) => ProfileScreen(),
  // "chat_screen": (BuildContext context) => ChatScreen(),
};
