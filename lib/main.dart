import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:happy_chat/presentation/routes/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: Color(0xFF161616), // navigation bar color
        statusBarColor: Colors.transparent,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: "splash",
      routes: routes,
      title: 'Happy Chat',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
